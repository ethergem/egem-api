const express = require('express')
const app = express();
const Web3 = require("web3");
const apiSettings = require("./configs/config.json");

web3 = new Web3(new Web3.providers.HttpProvider("https://lb.rpc.egem.io"));

var mysql = require('mysql');

var con = mysql.createPool({
  connectionLimit : 200,
  host: apiSettings.mysqlip,
  user: apiSettings.mysqluser,
  password: apiSettings.mysqlpass,
  database: apiSettings.mysqldb
});

app.get('/', (req, res) => {
  res.status(200).send({message: 'welcome to the ethergem API, visit our wiki to learn how to use it.'})
});

// https://api.egem.io/stats
app.get('/stats', (req, res) => {
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    connection.query("SELECT id,avgusd,currentBlock,currentSupply,totalNodes,currentMcap,tierOneNodes,tierTwoNodes,countedBotCoins FROM usersystems", function (err, data, fields){
      res.status(200).send({stats: data})
    })
    connection.release();
  });
});

// https://api.egem.io/longterm
app.get('/longterm', (req, res) => {
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    connection.query("SELECT avgusd,currentSupply,totalNodes,tierOneNodes,tierTwoNodes,dateTime FROM daily15", function (err, data, fields){
      res.status(200).send(data)
    })
    connection.release();
  });
});

// https://api.egem.io/dailyprice
app.get('/dailyprice', (req, res) => {
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    connection.query("SELECT avgusd FROM ( SELECT * from daily15 order by id desc LIMIT 96 ) tmp order by tmp.id asc", function (err, data, fields){
      let dataFinal = [];
      for (var i = 0; i < data.length; i++) {
        dataFinal.push(data[i]['avgusd'])
      }
      res.status(200).send(dataFinal)
    })
    connection.release();
  });
});

// https://api.egem.io/dailysupply
app.get('/dailysupply', (req, res) => {
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    connection.query("SELECT currentSupply FROM ( SELECT * from daily15 order by id desc LIMIT 96 ) tmp order by tmp.id asc", function (err, data, fields){
      let dataFinal = [];
      for (var i = 0; i < data.length; i++) {
        dataFinal.push(data[i]['currentSupply'])
      }
      res.status(200).send(dataFinal)
    })
    connection.release();
  });
});

// https://api.egem.io/dailynodes
app.get('/dailynodes', (req, res) => {
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    connection.query("SELECT totalNodes FROM ( SELECT * from daily15 order by id desc LIMIT 96 ) tmp order by tmp.id asc", function (err, data, fields){
      let dataFinal = [];
      for (var i = 0; i < data.length; i++) {
        dataFinal.push(data[i]['totalNodes'])
      }
      res.status(200).send(dataFinal)
    })
    connection.release();
  });
});

// https://api.egem.io/dailycreds
app.get('/dailycreds', (req, res) => {
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    connection.query("SELECT totalCredits FROM ( SELECT * from daily15 order by id desc LIMIT 96 ) tmp order by tmp.id asc", function (err, data, fields){
      let dataFinal = [];
      for (var i = 0; i < data.length; i++) {
        dataFinal.push(data[i]['totalCredits'])
      }
      res.status(200).send(dataFinal)
    })
    connection.release();
  });
});

// https://api.egem.io/dailymnrewards
app.get('/dailymnrewards', (req, res) => {
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    connection.query("SELECT totalMNRewards FROM ( SELECT * from daily15 order by id desc LIMIT 96 ) tmp order by tmp.id asc", function (err, data, fields){
      let dataFinal = [];
      for (var i = 0; i < data.length; i++) {
        dataFinal.push(data[i]['totalMNRewards'])
      }
      res.status(200).send(dataFinal)
    })
    connection.release();
  });
});

// https://api.egem.io/dailyt1
app.get('/dailyt1', (req, res) => {
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    connection.query("SELECT t1pay FROM ( SELECT * from daily15 order by id desc LIMIT 96 ) tmp order by tmp.id asc", function (err, data, fields){
      let dataFinal = [];
      for (var i = 0; i < data.length; i++) {
        dataFinal.push(data[i]['t1pay'])
      }
      res.status(200).send(dataFinal)
    })
    connection.release();
  });
});

// https://api.egem.io/dailyt2
app.get('/dailyt2', (req, res) => {
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    connection.query("SELECT t2pay FROM ( SELECT * from daily15 order by id desc LIMIT 96 ) tmp order by tmp.id asc", function (err, data, fields){
      let dataFinal = [];
      for (var i = 0; i < data.length; i++) {
        dataFinal.push(data[i]['t2pay'])
      }
      res.status(200).send(dataFinal)
    })
    connection.release();
  });
});

// https://api.egem.io/nodeuser?addr=
app.get('/nodeuser', function(req, res) {
  async function getNodeUser() {
    try {
      var user_addr = req.query.addr;
      var resultAddress = user_addr.match(/^0x[a-fA-F0-9]{40}$/)
      if (resultAddress != null) {
        con.getConnection(function(err, connection) {
          if (err) throw err; // not connected!
          // Show node user.
          connection.query("SELECT address,balance,mnrewards,userpay,lifetimepay,lastpayment FROM userdata WHERE address = ?",[user_addr], function (err, data, fields){
            if (err) {return res.status(200).send({nodeuser: "db error"})}

            if (data.length > 0) {
              res.status(200).send({
                address: data[0]["address"],
                chainbalance: Number(data[0]["balance"]),
                noderewards: Number(data[0]["mnrewards"]/Math.pow(10,18)),
                pay15min: Number(data[0]["userpay"]/Math.pow(10,18)),
                payhourly: Number(data[0]["userpay"]/Math.pow(10,18) * 4),
                paydaily: Number((data[0]["userpay"]/Math.pow(10,18) * 4) * 24),
                paylifetime: Number(data[0]["lifetimepay"]),
                lastpayment: data[0]["lastpayment"]
              })
            } else {
              return res.status(200).send({nodeuser: "no data to be found."})
            }

          })
          connection.release();
        })
      } else {
        res.status(200).send({nodeuser: "bad address lookup"})
      }
    } catch (e) {
      console.log(e)
    }
  }
  getNodeUser();
});

// https://api.egem.io/botusers
app.get('/botusers', (req, res) => {
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    connection.query("SELECT id,userId,address,balance FROM userdata", function (err, data, fields){
      res.status(200).send({profiles: data})
    })
    connection.release();
  });
});

// https://api.egem.io/estsupply
app.get('/estsupply', (req, res) => {
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    connection.query("SELECT currentSupply FROM usersystems", function (err, data, fields){
      res.status(200).send({estimated_supply: data[0]["currentSupply"]})
    })
    connection.release();
  });
});

// https://api.egem.io/summary
app.get('/summary', (req, res) => {
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    connection.query("SELECT * FROM usersystems", function (err, data, fields){
      res.status(200).send({settings: data})
    })
    connection.release();
  });
});

// https://api.egem.io/nodelist
app.get('/nodelist', (req, res) => {
  con.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    connection.query("SELECT userdata.id,userdata.userId,userdata.address,userdata.addressCreds,userdata.credits,userdata.mnrewards,userdata.balance,userdata.balanceTwo,userdata.earning,userdata.autopay,userdata.fails,userdata.notify,userdata.timestamp,userdata.avatar,userdata.lasttx,userdata.userpay,userdata.paylimit, usernodes1.count AS t1count, usernodes2.count AS t2count FROM userdata INNER JOIN usernodes1 ON userdata.id=usernodes1.id INNER JOIN usernodes2 ON usernodes1.id=usernodes2.id WHERE userdata.earning =1;", function (err, data, fields){
      res.status(200).send({profiles: data})
    })
    connection.release();
  });
});

// https://api.egem.io/account?addr=
app.get('/account', function(req, res) {
  async function getWalletBalance() {
    try {
      var user_addr = req.query.addr;
      var resultAddress = user_addr.match(/^0x[a-fA-F0-9]{40}$/)
      if (resultAddress != null) {
        var wallet = await web3.eth.getBalance(user_addr,function(error, result){
         if(!error){
           console.log("Results Forwarded: "+ user_addr)
           res.status(200).send({balance: result/Math.pow(10,18)})
         } else {
           res.status(200).send({balance: "bad address lookup"})
           console.error(error);
         }
        })
      } else {
        res.status(200).send({balance: "bad address lookup"})
      }
    } catch (e) {
      console.log(e)
    }
  }
  getWalletBalance();
});

// https://api.egem.io/tx?hash=
app.get('/tx', function(req, res) {
  async function getTXData() {
    try {
      var user_hash = req.query.hash;
      var resultTx = user_hash.match(/^0x[a-fA-F0-9]{64}$/)
      if (resultTx != null) {
        var tx = await web3.eth.getTransaction(user_hash, function(error, result){
          if(!error){
            console.log("Results Forwarded: "+ user_hash)
            res.status(200).send({tx: result})
          } else {
            res.status(200).send({tx: "bad tx lookup"})
            console.log(error);
          }
        })
      } else {
        res.status(200).send({tx: "bad tx lookup"})
      }
    } catch (e) {
      console.log(e)
    }
  }
  getTXData();
});

// https://api.egem.io/block?number=
// https://api.egem.io/block?hash=
app.get('/block', function(req, res) {
  async function getBlock() {
    try {
      if (req.query.number != null) {
        var block = req.query.number;
        var resultBlock = block.match(/^\d+$/)
      } else if (req.query.hash != null) {
        var block = req.query.hash;
        var resultBlock = block.match(/^0x[a-fA-F0-9]{64}$/)
      }
      if (resultBlock != null) {
        await web3.eth.getBlock(block, function(error, result){
          if(!error){
            console.log("Block " + block + " Result Forwarded.")
            res.status(200).send({block: result})
          } else {
            res.status(200).send({block: "bad lookup"})
            console.log(error);
          }
        })
      } else {
        res.status(200).send({block: "bad lookup"})
      }
    } catch (e) {
      console.log(e)
    }
  }
  getBlock();
});

// https://api.egem.io/uncle?number=&index=
// https://api.egem.io/uncle?hash=&index=
app.get('/uncle', function(req, res) {
  async function getUncle() {
    try {
      if (req.query.number != null) {
        var uncle = req.query.number;
        var resultUncle = uncle.match(/^\d+$/)
      } else if (req.query.hash != null) {
        var uncle = req.query.hash;
        var resultUncle = uncle.match(/^0x[a-fA-F0-9]{64}$/)
      }
      var index = req.query.index;
      var resultIndex = index.match(/^\d+$/)
      if (resultUncle != null && resultIndex != null) {
        await web3.eth.getUncle(uncle, index, function(error, result){
          if(!error){
            console.log("Uncle at Block " + uncle + " Index " + index + " Result Forwarded.")
            res.status(200).send({uncle: result})
          } else {
            res.status(200).send({uncle: "bad lookup"})
            console.log(error);
          }
        })
      } else {
        res.status(200).send({uncle: "bad lookup"})
      }
    } catch (e) {
      console.log(e)
    }
  }
  getUncle();
});

// https://api.egem.io/blocknumber
app.get('/blocknumber', function(req, res) {
  async function getBlockNumber() {
    try {
      await web3.eth.getBlockNumber(function(error, result){
        if(!error){
          console.log("Current Block Result Forwarded.")
          res.status(200).send({blocknumber: result})
        } else {
          res.status(200).send({blocknumber: "error"})
          console.log(error);
        }
      })
    } catch (e) {
      console.log(e)
    }
  }
  getBlockNumber();
});

// https://api.egem.io/transactions?addr=
app.get('/transactions', function(req, res) {
  async function getTransactions() {
    try {
      var address = req.query.addr;
      var resultAddress = address.match(/^0x[a-fA-F0-9]{40}$/)
      if (resultAddress != null) {
        await web3.eth.getTransactionCount(address, function(error, result){
          if(!error){
            console.log("Transactions For Account " + address + " Result Forwarded.")
            res.status(200).send({transactions: result})
          } else {
            console.log(error);
          }
        })
      } else {
        res.status(200).send({transactions: "bad lookup"})
      }
    } catch (e) {
      console.log(e)
    }
  }
  getTransactions();
});

// https://api.egem.io/code?addr=
app.get('/code', function(req, res) {
  async function getCode() {
    try {
      var address = req.query.addr;
      var resultAddress = address.match(/^0x[a-fA-F0-9]{40}$/)
      if (resultAddress != null) {
        await web3.eth.getCode(address, function(error, result){
          if(!error){
            console.log("Code At Address " + address + " Result Forwarded.")
            res.status(200).send({code: result})
          } else {
            res.status(200).send({code: "bad lookup"})
            console.log(error);
          }
        })
      } else {
        res.status(200).send({code: "bad lookup"})
      }
    } catch (e) {
      console.log(e)
    }
  }
  getCode();
});

// https://api.egem.io/sendsigned?hex=
app.get('/sendsigned', function(req, res) {
  async function sendSigned() {
    try {
      var serializedTx = req.query.hex;
      var resultHex = serializedTx.match(/^0x[a-fA-F0-9]+$/)
      if (resultHex != null) {
        await web3.eth.sendSignedTransaction(serializedTx.toString('hex'), function(error, result){
          if(!error){
            console.log("Sent Signed Transaction.")
            res.status(200).send({signed: result})
          } else {
            res.status(200).send({signed: "bad signed message"})
            console.log(error);
          }
        })
      } else {
        res.status(200).send({signed: "bad signed message"})
      }
    } catch (e) {
      console.log(e)
    }
  }
  sendSigned();
});

// https://api.egem.io/gas
app.get('/gas', function(req, res) {
  async function sendSigned() {
    try {
      res.status(200).send({"fast": 200.0, "fastest": 210.0, "safeLow": 100.0, "average": 100.0, "block_time": 12.25, "blockNum": 0, "speed": 0.0025504000000000004, "safeLowWait": 8.3, "avgWait": 8.3, "fastWait": 4.4, "fastestWait": 4.3})
    } catch (e) {
      console.log(e)
    }
  }
  sendSigned();
});

app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' API method not found, try another one.'})
});

const server = app.listen(8081, () => {
  console.log(`EtherGem API running → PORT ${server.address().port}`);
});
